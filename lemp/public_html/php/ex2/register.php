<?php
    if(isset($_POST['submit']))
    {
        // User Data
        $usr = $_POST['username'];
        $psw = $_POST['password'];
        // Hashed PSW
        $hash = password_hash($psw, PASSWORD_DEFAULT);

        if (password_verify($psw, $hash)) 
        {
            // Delete Password
            unset($psw);
            // Connection to the DB
            $host = 'localhost';
            $dbuser = 'admin';
            $dbpsw = 'password';
            $dbname = 'webserver';
            $dbtable = 'users';

            $link = mysqli_connect($host, $dbuser, $dbpsw, $dbname, 8080) or die('DB Connection Error');
            if ($link)
            {
                // Query
                $sql = "INSERT INTO users(username, password) 
                VALUES ('$usr', '$hash')";
                // Query Execution
                mysqli_query($link, $sql);
            }
        }
        else
        {
            echo "Password has not been correctly hashed, try again later";
        }
    }
?>