<?php

  $username = $_POST['username'];

  echo "Hello $username <br>";

   $age = array(
     "Peter"=>"35",
     "Ben"=>"37",
     "Joe"=>"43"
   );

   echo "Peter is " . $age['Peter'] . " years old. <br>";

  if (true)
  {
    # code...
    echo "If (true) Statement <br>";
  }
  if (false) {
    # not executed code...
  } else {
    # code...
    echo "If (false) else Statement <br>";
  }

  for ($i=0; $i < 5; $i++)
  {
    # code...
    echo "Hello i: $i \n";
  }

  $a = 0;
  while ($a < 5)
  {
    # code...
    echo "Hello a: $a \n";
    $a += 1;
  }

  do
  {
    # code...
    echo "Hello a: $a \n";
    $a += 1;
  } while ($a <= 5);

  switch ($variable)
  {
    case 'value':
      # code...
      break;
    case 4:
      # code...
      break;
    default:
      # code...
      break;
  }

  $arr = array(1, 2, 3, 4);
  foreach ($arr as &$value)
  {
      $value = $value * 2;
  }
  // $arr is now array(2, 4, 6, 8)

  // without an unset($value), $value is still a reference to the last item: $arr[3]

  foreach ($arr as $key => $value)
  {
      // $arr[3] will be updated with each value from $arr...
      echo "{$key} => {$value} ";
      echo "$arr";
  }

  function FunctionName($value='', $var2)
  {
    # code...
    return $value;
  }

  global $var = '';
  // ==== Constants ==== //
  define("PI", 3.14);
  define('MIN_VALUE', '0.0');   // RIGHT - Works OUTSIDE of a class definition.
  define('MAX_VALUE', '1.0');   // RIGHT - Works OUTSIDE of a class definition.

  const MIN_VALUE = 0.0;        // RIGHT - Works both INSIDE and OUTSIDE of a class definition.
  const MAX_VALUE = 1.0;        // RIGHT - Works both INSIDE and OUTSIDE of a class definition.

  class Constants
  {
    //define('MIN_VALUE', '0.0');  WRONG - Works OUTSIDE of a class definition.
    //define('MAX_VALUE', '1.0');  WRONG - Works OUTSIDE of a class definition.

    const MIN_VALUE = 0.0;      // RIGHT - Works INSIDE of a class definition.
    const MAX_VALUE = 1.0;      // RIGHT - Works INSIDE of a class definition.

    public static function getMinValue()
    {
      return self::MIN_VALUE;
    }

    public static function getMaxValue()
    {
      return self::MAX_VALUE;
    }
  }

  //==== Built-in functions ====//
  pow($b,$e);
  rand($min, $max);
  sqrt($r);
  ceil($n);
  
 ?>
