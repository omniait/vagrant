<?php
    class Cart
    {
        public $items = array();
        public $q = array();

        public function __construct($items, $q)
        {
            $this->items = $items;
            $this->q = $q;            
        }

        function remove()
        {
            # code...
        }
        function add()
        {

        }
        function update()
        {
            # code...
        }
        public function stamp()
        {
            for ($i=0; $i < sizeof($this->items); $i++) 
            { 
                echo "<b> Articolo </b> ".$this->items[$i]."<br>";
                echo "<b> Quantità </b> ".$this->q[$i]."<br>";
            }
        }
    }

    class Checkout extends Cart
    {
        public $prices = array();

        public function __construct($items, $q, $prices)
        {
            parent::__construct($items, $q);
            $this->prices = $prices;
        }

        public function stamp()
        {
            for ($i=0; $i < sizeof($this->items); $i++) 
            { 
                echo "<b> Articolo </b> ".$this->items[$i]."<br>";
                echo "<b> Quantità </b> ".$this->q[$i]."<br>";
                echo "<b> Prezzo </b> ".$this->prices[$i]." € <br>";                
            }
        }
    }
    
    $Cart = new Checkout(["Paperella","Mouse"],[40,50],[150,10]);

    include "index.html";

    $Cart->stamp();
?>