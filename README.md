# Vagrant Boxes

Available Boxes

* Centos:
  * config.vm.box = "centos/7"
* Debian:
  * config.vm.box = "debian/jessie64"
* Alpine:
  * Self Box Construction

## LEMP Stack

### Short Description

A LEMP stack is made by Linux (one of the boxes provided), nginx (Engine X), MySql (MariaDB), PHP.

### Debian packages

* php7.0 php7.0-fpm php7.0-mysql
* mariadb-client mariadb-server
* nginx

### nginx Config:

```
server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;
    root /vagrant;
    index index.php index.html index.htm;
    server_name server_domain_or_IP;
    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }
    location ~ \.php\$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)\$;
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_
        name;
        include fastcgi_params;
    }
}
```

## VSCode SFTP file configuration:

```json
{
    "protocol": "sftp",
    "host": "127.0.0.1",
    "port": 2222,
    "username": "vagrant",
    "privateKeyPath": "absolute + /vagrant/lemp/.vagrant/machines/default/virtualbox/private_key",
    "remotePath": "/vagrant"
}
```

